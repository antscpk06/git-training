<?php

namespace Tests\Unit\Services;

use App\Http\Repositories\TodoRepository;
use Tests\TestCase;
use Mockery\MockInterface;
use App\Http\Services\TodoService;
use Mockery;
use App\Models\Todo;
use Exception;
use Illuminate\Http\Request;


class TodoSTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetTodos()
    {
        $mock = Mockery::mock('overload:'.Todo::class)->makePartial();
        $mock->shouldReceive('latest')->andReturnSelf();
        $mock->shouldReceive('get')->andReturn([]);

        $this->app->instance(Todo::class, $mock);
        $todos = (new TodoService)->GetTodos();
        $this->assertEquals(0,count($todos));
    }

    public function testSimple()
    {
        $sim = (new TodoService)->simpleCalculate(1,2);

        $this->assertEquals(2,$sim);
    }

    public function testStore()
    {
        $mock = Mockery::mock('overload:'.TodoRepository::class)->makePartial();
        $mock->shouldReceive('store')->andReturn(((object)['name'=>'Ha']));
        $req = new Request;
        $req['name'] = 'Ha';
        $actual = (new TodoService)->storeTodo($req);
        $this->assertEquals($actual->name, 'Ha');
    }
}
