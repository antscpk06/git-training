<?php

namespace Tests\Unit\Services;

use PHPUnit\Framework\TestCase;
use Mockery\MockInterface;
use App\Http\Services\TodoService;
use Mockery;
use App\Models\Todo;

class TodoServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetTodos()
    {
        Mockery::mock(Todo::class, function (MockInterface $mock) {
            $mock->shouldReceive('get')->once()
            ->andReturn([]);
        });
        $todos = (new TodoService)->GetTodos();
        var_dump($todos);
        $this->assertEquals([],$todos);
    }
}
