<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Todo;
use App\Http\Resources\TodoResource;
use Validator;
use App\Http\Services\TodoService;


class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TodoService $service)
    {
        return response()->json([$service->GetTodos(), 'Todos fetched.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'desc' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $Todo = Todo::create([
            'name' => $request->name,
            'desc' => $request->desc
        ]);

        return response()->json(['Todo created successfully.', new TodoResource($Todo)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Todo = Todo::find($id);
        if (is_null($Todo)) {
            return response()->json('Data not found', 404);
        }
        return response()->json([new TodoResource($Todo)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $Todo)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'desc' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $Todo->name = $request->name;
        $Todo->desc = $request->desc;
        $Todo->save();

        return response()->json(['Todo updated successfully.', new TodoResource($Todo)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $Todo)
    {
        $Todo->delete();

        return response()->json('Todo deleted successfully');
    }
}
