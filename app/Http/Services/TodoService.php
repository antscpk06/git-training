<?php

namespace App\Http\Services;

use App\Http\Repositories\TodoRepository;
use App\Models\Todo;
use App\Http\Resources\TodoResource;
use Illuminate\Http\Request;

class TodoService
{
    /**
     * @return mixed
     */
    public function GetTodos(): mixed
    {
        $result = Todo::latest()->get();
        return TodoResource::collection($result);
    }

    public function simpleCalculate(int $a, int $b)
    {
        return $a + $b - $a;
    }

    public function storeTodo(Request $request)
    {
        $todo = (new TodoRepository)->store((object) $request);
        return new TodoResource($todo);
    }
    
}
